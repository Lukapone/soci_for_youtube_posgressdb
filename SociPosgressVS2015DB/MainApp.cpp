#include "soci.h"
#include "soci-postgresql.h"
#include <iostream>
#include <istream>
#include <ostream>
#include <string>
#include <exception>

using namespace soci;
using namespace std;

int main()
{
	try
	{
		session sql(postgresql, "dbname=GameDB user=postgres password=password");
		sql << "DROP TABLE customers";
		sql << "CREATE TABLE customers(cust_id int NOT NULL, cust_name varchar(20), cust_password varchar(64) NOT NULL, PRIMARY KEY (cust_name))";

		std::string name{ "AAukas" };
		std::string pass{ "Lukas" };
		int c_ID{ 0 };
		sql << "insert into customers(cust_id, cust_name, cust_password) "
			"values(:cust_id, :cust_name, :cust_password)", use(c_ID), use(name), use(pass);
		

	}
	catch (exception const &e)
	{
		cerr << "Error: " << e.what() << '\n';
	}

	system("PAUSE");
}